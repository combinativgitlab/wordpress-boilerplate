# Wordpress Boilerplate

Wordpress + PHPMyAdmin + Mysql
## SETUP instructions:
- docker-compose up -d
- [tear down volumes] docker-compose down -v 
## Wordpress Credentials:

- ### Wordpress

  - WORDPRESS_DB_USER: wordpress
  - WORDPRESS_DB_PASSWORD: wordpress

- ### PHPMyAdmin

  - PMA_HOST: db
  - MYSQL_ROOT_PASSWORD: password

- ### Mysql

  - MYSQL_USER: wordpress
  - MYSQL_PASSWORD: wordpress
  - MYSQL_ROOT_PASSWORD: password
